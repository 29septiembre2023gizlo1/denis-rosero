package proyecto_algoritmo2;

import java.util.Scanner;

public class PROYECTO_ALGORITMO2 {

    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        System.out.println("Ingrese identificación:\t");
        String identificacion = leer.next();
        String tipo = "";
        String provincia = "";
        if(identificacion.length()==10 || identificacion.length()==13){
            if(identificacion.length()==10){
                tipo = "CEDULA";
            }
            if(identificacion.length()==13){
                tipo = "RUC";
            }
            if(identificacion.substring(0, 2).equals("09")){
                provincia = "GUAYAQUIL";
            }else{
                provincia = "OTRA PROVINCIA";
            }
            System.out.println(tipo+" DE "+provincia);
        }else{
            System.out.println("IDENTIFICACIÓN INVALIDA");
        }
    }
    
}
