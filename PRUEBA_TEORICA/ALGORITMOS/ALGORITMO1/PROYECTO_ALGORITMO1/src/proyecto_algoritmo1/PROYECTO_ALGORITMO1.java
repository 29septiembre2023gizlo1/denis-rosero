package proyecto_algoritmo1;
import java.security.SecureRandom;

public class PROYECTO_ALGORITMO1 {
    public static void main(String[] args) {
        SecureRandom random = new SecureRandom();
        StringBuilder clave = new StringBuilder();
        int longitud = random.nextInt(8) + 8; // entre 8,15 caracteres
        while (clave.length() < longitud) {
            int tipoCaracter = random.nextInt(3); // 0 mayúscula, 1 minúscula, 2 especial
            switch (tipoCaracter) {
                case 0 -> clave.append((char) (random.nextInt(26) + 'A')); // mayúscula
                case 1 -> clave.append((char) (random.nextInt(26) + 'a')); // minúscula
                case 2 -> clave.append((char) (random.nextInt(15) + 33)); // carácter especial
            }
        }
        System.out.println("Clave generada:\t\t"+clave.toString());
    }
    
}
