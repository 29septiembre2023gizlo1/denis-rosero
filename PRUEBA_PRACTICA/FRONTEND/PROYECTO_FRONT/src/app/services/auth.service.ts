import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private userKey = 'user';
  private isAuthenticated: boolean = false;
  private user: any = null;

  constructor() {
    // Al iniciar la aplicación, verifica si el usuario ya está autenticado en localStorage
    const storedUser = localStorage.getItem(this.userKey);
    if (storedUser) {
      this.isAuthenticated = true;
      this.user = JSON.parse(storedUser);
    }
  }

  login(email: string, password: string): boolean {
    // Aquí deberías realizar la lógica de autenticación real
    // Si la autenticación es exitosa, establece isAuthenticated en true y guarda la información del usuario.
    if (email === 'dennis@company.com' && password === '123') {
      this.isAuthenticated = true;
      this.user = { email: email };
      localStorage.setItem(this.userKey, JSON.stringify(this.user)); // Guarda el usuario en localStorage
      return true;
    }
    return false;
  }

  logout(): void {
    // Cierra la sesión del usuario y elimina la información del usuario de localStorage
    this.isAuthenticated = false;
    this.user = null;
    localStorage.removeItem(this.userKey);
  }

  isLoggedIn(): boolean {
    return this.isAuthenticated;
  }

  getUser(): any {
    return this.user;
  }
}