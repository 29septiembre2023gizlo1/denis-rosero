import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { UsuarioComponent } from './pages/usuario/usuario.component';
import { AuthGuard } from './../app/services/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: '/list-users', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'list-users', component: UsuarioComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
