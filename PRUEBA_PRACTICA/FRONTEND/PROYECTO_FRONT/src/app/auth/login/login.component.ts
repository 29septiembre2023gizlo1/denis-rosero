import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  user = {
    email: '',
    password: ''
  };

  constructor(private router: Router, private authService: AuthService) {}

  onLoginSubmit() {
    if (this.authService.login(this.user.email, this.user.password)) {
      // Autenticación exitosa, redirige al usuario a la ruta principal
      this.router.navigate(['/list-users']); // Ajusta la ruta según tu estructura de rutas
    } else {
      // Manejo de error de autenticación (puedes mostrar un mensaje de error)
      console.log('Autenticación fallida. Por favor, verifica tus credenciales.');
    }
  }

}
